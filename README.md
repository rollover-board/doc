# Rollover a simple messageboard application
Some documentation for the rollover chat system

## Components
In order of dependency these are the service components:

* rolloverproto     - grpc service protocol definitions
* Filterengine      - prediction model to power the filterservice
* FilterService     - spam and unwanted message filtering.
* PostService       - microservice which handles message posts
* Ui                - react web frontend to the post service.
* dockerutilities   - container of tools to debug issues
* rollover-k8s      - kubernetes deployment

## Building
Here's a typical setup to build the entire project.
Projects are checked out in order of dependency.

```
mkdir rollover
cd rollover
git clone git@gitlab.com:rollover-board/rolloverproto.git
git clone git@gitlab.com:rollover-board/filterengine.git
git clone git@gitlab.com:rollover-board/filterservice.git
git clone git@gitlab.com:rollover-board/postservice.git
git clone git@gitlab.com:rollover-board/ui.git
git clone git@gitlab.com:tplsofteng/dockerutilities.git
git clone git@gitlab.com:rollover-board/rollover-k8s.git
```
